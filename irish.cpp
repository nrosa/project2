// NETID(s):nrosa and hgebreme 
#include <sys/types.h>
#include <zmq.hpp>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <time.h>
#include <string>
#include <sstream>
#include <map>
#include <sys/wait.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <list>

using namespace std;

void mySignalHandler (int);
void usage();
void quit();
void level1();
void level2();
void shellUsage();

struct Process{ // Process struct  
	int pid;
	string state;
	bool bg;
};

void status( list<Process> processList ); // checks the status of programs in our list and updates them 


int main(int argc, char *argv[]){
	
	
	if (argc != 3){
		cout<<"Incorrect number of arguments. Try again. "<<endl;
		usage();
		exit(EXIT_FAILURE);
	}

	if(strcmp(argv[2], "1") == 0){
		level1();
	}
	else if(strcmp(argv[2], "2")==0){
		level2();
	}
	else{
		cout<<"Invalid arguments. Try again. "<<endl;
		usage();
		exit(EXIT_FAILURE);
	}

}






//--------------------------FUNCTIONS-------------------------------//

void level1(){
	//Catch control-c via signal handler
	signal (SIGINT, mySignalHandler); // if SIGINT occurs, the handler of the code is invoked 
	// there are many types of signals you can react to
	
	list<Process> processList;

	// dictionary that maps from signal call to the corresponding integer value
	map<string, int> converterMap;
	converterMap["SIGHUP"] =  1;
	converterMap["SIGINT"] =  2;
	converterMap["SIGQUIT"] = 3;
	converterMap["SIGSTOP"] = 19;
	converterMap["SIGKILL"] = 9;
	converterMap["SIGALARM"] = 14;
	converterMap["SIGCONT"] =  18;

	cout<<"Attempting to irish. Start adding commands whenever you'd like."<<endl;	

	while (1){
		// Taking in commands from cin 
		string command;
		getline(cin, command);
		char * newCommand = new char [command.length()+1];
		strcpy (newCommand, command.c_str());
		char * pch = strtok(newCommand, " "); // breaking up command using the space delimitter 
		int i=0;
		string first = pch;					  //first == initial command (limited options)

		//-------------------- list ---------------------// list command implementation

		if(strcmp(first.c_str(), "list")==0){ //help message
		int status = 0;
		bool nonFG = false;
		for (list<Process>::iterator it=processList.begin(); it != processList.end(); it++){
			if ((*it).state == "Ended" || (*it).bg ==false){
				continue;
			}
			int r = waitpid((*it).pid, &status, WNOHANG);
	
				if (r==0){
														// If the process hasn't ended, leave the state as is 
				}	
				else if (r == -1){
					cout<<"Error trying to get the state of process: "<<(*it).pid<<endl;
				}
				else {									//returns something other than 0 or -1 if the process hasn't ended
					(*it).state = "Ended"; 
				}
				if ((*it).bg == true){
					nonFG = true;						//Just used to track if there is anything in the list that hasn't been moved to fg
				}
	
		}

		//Now go through and print everything properly
			if (nonFG == true){				//As long as there is at least one process that is running or has completed in the background
				if (processList.size()== 1){
					cout<<processList.size()<<" Background process."<<endl;
					int count = 0;
					for (list<Process>::iterator it=processList.begin(); it != processList.end(); it++){
						count++;
						cout<<"("<<count<<")"<<" PID="<<(*it).pid<<" State="<<(*it).state<<endl;
					}
				}
				else{
					cout<<processList.size()<<" Background processes."<<endl;
					int count = 0;
					for (list<Process>::iterator it=processList.begin(); it != processList.end(); ++it){
						count++;
						cout<<"("<<count<<")"<<" PID="<<(*it).pid<<" State="<<(*it).state<<endl;
					}
				}
			}
			else{
				cout<<"No subprocesses."<<endl;
			}
		}

		//-------------------- bg ---------------------// bg command implementation

		else if(strcmp(first.c_str(), "bg")==0){
			char * nullArray[100];
			while ((pch = strtok(NULL, " "))){
				nullArray[i] = pch;
				i++;
			}
			nullArray[i] = NULL;
			if (!nullArray[0]){
				cout<<"bg requires at least one argument"<<endl;
				shellUsage();
				continue;
			}

			int rc = fork();
			Process childProcess;
			

			if (rc<0){
				//fork failed; exit
				fprintf(stderr, "fork failed\n");
				exit(EXIT_FAILURE);
			}
			else if(rc ==0){
				//child process
					execvp(nullArray[0], nullArray);

			}
			else{
				childProcess.bg = true;
				childProcess.state = "Running";
				childProcess.pid = rc;  
				processList.push_back(childProcess);

			}
		}
		
		//-------------------- fg ---------------------// fg command implementation

		else if(strcmp(first.c_str(),"fg")==0){
			string Arr[10];
			while ((pch = strtok(NULL, " "))){
				Arr[i] = pch;
				i++;
			}
			if (Arr[0]==""){
				cout<<"fg needs one argument"<<endl;
				shellUsage();
				continue;
			}
			int status = 0;
			int r = waitpid(stoi(Arr[0]), &status, 0);
			if (r == -1){
				cout<<"There was an error moving that process to the fg. Make sure it is a running process by typing 'list'. "<<endl;
				continue;
			}
			for (list<Process>::iterator it=processList.begin(); it != processList.end(); ++it){
				if	((*it).pid == stoi(Arr[0])){
					(*it).bg = false;
				}
			}	
		}

		//-------------------- signal ---------------------// signal command implementation

		else if(strcmp(first.c_str(), "signal")==0){

			string Arr[10];
			while ((pch = strtok(NULL, " "))){
				Arr[i] = pch;
				i++;
			}
			if (Arr[0]==""){
				cout<<"signal needs two arguments"<<endl;
				shellUsage();
				continue;
			}
			auto search = converterMap.find(Arr[1]);
			if (search != converterMap.end()){

			
				int result = kill(stoi(Arr[0]), search->second);
				if (result != 0){
					cout<<"There was an issue signaling the process: "<<strerror(errno)<<endl;
				}
				else{
					cout<<"Process "<<stoi(Arr[0])<<" sent"<<Arr[1]<<endl;
					for (list<Process>::iterator it=processList.begin(); it != processList.end(); ++it){
						if	((*it).pid == stoi(Arr[0])){
							(*it).state = "Running";
						}
					}	
				}
			}

		}

		//-------------------- stop ---------------------// stop command implementation

		else if(strcmp(first.c_str(), "stop")==0){
			string Arr[10];
			while ((pch = strtok(NULL, " "))){
				Arr[i] = pch;
				i++;
			}
			if (Arr[0]== ""){
				cout<<"stop needs one argument"<<endl;
				shellUsage();
				continue;
			}

			int result = kill(stoi(Arr[0]), 19);
			if (result != 0){
				cout<<"There was an issue signaling the process: "<<strerror(errno)<<endl;
			}
			else{
				cout<<"Process "<<stoi(Arr[0])<<" stopped."<<endl;
				for (list<Process>::iterator it=processList.begin(); it != processList.end(); ++it){
					if	((*it).pid == stoi(Arr[0])){
						(*it).state = "Stopped";
					}
				}

			}

		}

		//-------------------- continue ---------------------// continue command implementation

		else if(strcmp(first.c_str(), "continue")==0){
			string Arr[10];
			while ((pch = strtok(NULL, " "))){
				Arr[i] = pch;
				i++;
			}
				
			if (Arr[0]==""){
				cout<<"continue needs one argument"<<endl;
				shellUsage();
				continue;
			}

			int result = kill(stoi(Arr[0]), 18);
			if (result != 0){
				cout<<"There was an issue signaling the process: "<<strerror(errno)<<endl;
			}
			else{
				cout<<"Process "<<stoi(Arr[0])<<" continued."<<endl;
				for (list<Process>::iterator it=processList.begin(); it != processList.end(); ++it){
					if	((*it).pid == stoi(Arr[0])){
						(*it).state = "Running";
					}
				}

			}
		}

		//-------------------- quit ---------------------// quit command implementation

		else if(strcmp(first.c_str(), "quit")==0){
			quit();
		}
		
		//-------------------- command not found case ---------------------// display error and shell usage

		else{
			cout<< "That is not one of the available commands! Try one of these: "<<endl;
			shellUsage();
		}
			 
	}

} 

void level2(){

	// zmq initializations to get commands from the python server
	
	zmq::context_t context (1);
	zmq::socket_t  subscriber(context, ZMQ_SUB);
	string port= "62400"; // lowest port number assigned to us


	cout<<" Receiving strings from Irish.py"<<endl;

	subscriber.connect("tcp://localhost:"+port);
	subscriber.setsockopt(ZMQ_SUBSCRIBE, " ", 0);
	
	//Level 2 implementation is mostly similar to Level 1 except for how the input is received
	//Catch control-c via signal handler
	signal (SIGINT, mySignalHandler); // if SIGINT occurs, the handler of the code is invoked 

	
	list<Process> processList;
	map<string, int> converterMap;
	converterMap["SIGHUP"] =  1;
	converterMap["SIGINT"] =  2;
	converterMap["SIGQUIT"] = 3;
	converterMap["SIGSTOP"] = 19;
	converterMap["SIGKILL"] = 9;
	converterMap["SIGALARM"] = 14;
	converterMap["SIGCONT"] =  18;

	cout<<"Attempting to irish. Start adding commands whenever you'd like."<<endl;	
	while (1){

		// we get commands from the Python side
		zmq::message_t reply;
		subscriber.recv(&reply, 0);
		string command = string(static_cast<char*>(reply.data()), reply.size()); // this is the string command passed to level 1 function
		cout<< command <<endl;
		// similar level 1 implementation without cin but using zmq string received 
		char * newCommand = new char [command.length()+1];
		strcpy (newCommand, command.c_str());
		char * pch = strtok(newCommand, " "); 
		int i=0;
		string first = pch;

		//-------------------- list ---------------------// list command implementation

		if(strcmp(first.c_str(), "list")==0){ //help message
		int status = 0;
		bool nonFG = false;
		for (list<Process>::iterator it=processList.begin(); it != processList.end(); it++){
			if ((*it).state == "Ended" || (*it).bg ==false){
				continue;
			}
			int r = waitpid((*it).pid, &status, WNOHANG);
	
				if (r==0){
					// what do we need here 
				}	
				else if (r == -1){
					cout<<"Error trying to get the state of process: "<<(*it).pid<<endl;
				}
				else {
					(*it).state = "Ended"; 
				}
				if ((*it).bg == true){
					nonFG = true;
				}
	
		}

		//Now go through and print everything properly
			if (nonFG == true){						//As long as there is at least one process that is running or has completed in the background
				if (processList.size()== 1){
					cout<<processList.size()<<" Background process."<<endl;
					int count = 0;
					for (list<Process>::iterator it=processList.begin(); it != processList.end(); it++){
						count++;
						cout<<"("<<count<<")"<<" PID="<<(*it).pid<<" State="<<(*it).state<<endl;
					}
				}
				else{
					cout<<processList.size()<<" Background processes."<<endl;
					int count = 0;
					for (list<Process>::iterator it=processList.begin(); it != processList.end(); ++it){
						count++;
						cout<<"("<<count<<")"<<" PID="<<(*it).pid<<" State="<<(*it).state<<endl;
					}
				}
			}
			else{
				cout<<"No subprocesses."<<endl;
			}
		}

		//-------------------- bg ---------------------// bg command implementation


		else if(strcmp(first.c_str(), "bg")==0){
			char * nullArray[100];
			while ((pch = strtok(NULL, " "))){
				nullArray[i] = pch;
				i++;
			}
			nullArray[i] = NULL;

			if (!nullArray[0]){
				cout<<"bg requires at least one argument"<<endl;
				shellUsage();
				continue;
			}
			int rc = fork();
			
			Process childProcess;

			if (rc<0){
				//fork failed; exit
				fprintf(stderr, "fork failed\n");
				exit(EXIT_FAILURE);
			}
			else if(rc ==0){
				//child process
				execvp(nullArray[0], nullArray);

			}
			else{
		
				childProcess.bg = true;
				childProcess.state = "Running";
				childProcess.pid = rc; 
				cout<< "Process "<<rc<<" was started in background"<<endl; 
				processList.push_back(childProcess);

			}
		} 

		//-------------------- fg ---------------------// fg command implementation

		else if(strcmp(first.c_str(),"fg")==0){
			string Arr[10];
			while ((pch = strtok(NULL, " "))){
				Arr[i] = pch;
				i++;
			}
			if (Arr[0]==""){
				cout<<"fg needs one argument"<<endl;
				shellUsage();
				continue;
			}

			int status = 0;
			int r = waitpid(stoi(Arr[0]), &status, 0);
			if (r == -1){
				cout<<"There was an error moving that process to the fg. Make sure it is a running process by typing 'list'. "<<endl;
				continue;
			}
				for (list<Process>::iterator it=processList.begin(); it != processList.end(); ++it){
					if	((*it).pid == stoi(Arr[0])){
						(*it).bg = false;
					}
				}	
		}

		//-------------------- signal ---------------------// signal command implementation

		else if(strcmp(first.c_str(), "signal")==0){

			string Arr[10];
			while ((pch = strtok(NULL, " "))){
				Arr[i] = pch;
				i++;
			}
			if (Arr[0]=="" || Arr[1]==""){
				cout<<"signal needs one argument"<<endl;
				shellUsage();
				continue;
			}

			auto search = converterMap.find(Arr[1]);
			if (search != converterMap.end()){

			
				int result = kill(stoi(Arr[0]), search->second);
				if (result != 0){
					cout<<"There was an issue signaling the process: "<<strerror(errno)<<endl;
				}
				else{
					cout<<"Process "<<stoi(Arr[0])<<" sent "<<Arr[1]<<endl;
					for (list<Process>::iterator it=processList.begin(); it != processList.end(); ++it){
						if	((*it).pid == stoi(Arr[0])){
							(*it).state = "Running";
						}
					}	
				}
			}

		}

		//-------------------- stop ---------------------// stop command implementation

		else if(strcmp(first.c_str(), "stop")==0){
			string Arr[10];
			while ((pch = strtok(NULL, " "))){
				Arr[i] = pch;
				i++;
			}
			if (Arr[0]==""){
				cout<<"stop needs one argument"<<endl;
				shellUsage();
				continue;
			}


			int result = kill(stoi(Arr[0]), 19);
			if (result != 0){
				cout<<"There was an issue signaling the process: "<<strerror(errno)<<endl;
			}
			else{
				cout<<"Process "<<stoi(Arr[0])<<" stopped."<<endl;
				for (list<Process>::iterator it=processList.begin(); it != processList.end(); ++it){
					if	((*it).pid == stoi(Arr[0])){
						(*it).state = "Stopped";
					}
				}

			}

		}

		//-------------------- continue ---------------------// continue command implementation

		else if(strcmp(first.c_str(), "continue")==0){
			string Arr[10];
			while ((pch = strtok(NULL, " "))){
				Arr[i] = pch;
				i++;
			}
			if (Arr[0]==""){
				cout<<"continue needs one argument"<<endl;
				shellUsage();
				continue;
			}


			int result = kill(stoi(Arr[0]), 18);
			if (result != 0){
				cout<<"There was an issue signaling the process: "<<strerror(errno)<<endl;
			}
			else{
				cout<<"Process "<<stoi(Arr[0])<<" continued."<<endl;
				for (list<Process>::iterator it=processList.begin(); it != processList.end(); ++it){
					if	((*it).pid == stoi(Arr[0])){
						(*it).state = "Running";
					}
				}

			}
		}

		//-------------------- quit ---------------------// quit command implementation

		else if(strcmp(first.c_str(), "quit")==0){
			quit();
		}

		//-------------------- command not found  ---------------------// display error and show shell usage

		else{
			cout<< "That is not one of the available commands! Try one of these: "<<endl;
			shellUsage();
		}
			 
	}

		

}

void status( list<Process> processList){ // iterates over the process list and updates status of the 
	for (list<Process>::iterator it=processList.begin(); it != processList.end(); ++it){
		int r = kill((*it).pid, 0);
	
			if (r==0){
				cout<<"in if"<<endl;
				(*it).state = "Running"; 
			}	
			else{
				cout<<"in the else"<<endl;
				(*it).state = "Ended";
			 
			}
	
	}

}

void shellUsage(){
	cout<<"list: prints a list of active child processes"<<endl;
	cout<<"bg COMMAND: executes an external command COMMAND asynchronously in the background"<<endl;
	cout<<"fg PID: take a previously running command in the background and wait for it to terminate, collecting the return code"<<endl;
	cout<<"signal PID SIGNAL: signals process PID an arbitrary SIGNAL"<<endl;
	cout<<"stop PID: signals process PID to stop; an alias for signal PID SIGSTOP"<<endl;
	cout<<"continue PID: signals process PID to continue; an alias for signal PID SIGSTOP"<<endl;
	cout<<"quit: manually exit, forcibly terminate any remaining child processes"<<endl;
}


void mySignalHandler(int sig){
	//There is not much to clean up because we are using C++  which has clean up mechanisms employed for when the program ends. 
	//Cleans up the ZMQ context and sockets as well as any memory allocated over the course of the program
	
	cout<<endl<<"Graceful exit!"<<endl; 
	exit(EXIT_SUCCESS);


}

void usage(){
	cout<<"The commands for irish are as follows: "<<endl;
	cout<<"irish -level 1: runs level one of the project (A non-networked version of the daemon)"<<endl;
	cout<<"irish -level 2: runs level two of the project (A basic ZMQ version of the daemon)"<<endl;

}

void quit(){
	cout<<"Thanks for using irish! Exiting... "<<endl;
	exit(EXIT_SUCCESS);
}

