# DO NOT EDIT -------------------------------
C=/afs/nd.edu/user14/csesoft/new/bin/gcc
CFLAGS=-Wall
CPP=/afs/nd.edu/user14/csesoft/new/bin/g++
CPPFLAGS=-Wall
LD=/afs/nd.edu/user14/csesoft/new/bin/g++
LDFLAGS=-L/afs/nd.edu/user14/csesoft/2018-spring/lib -lzmq
INCLUDE=/afs/nd.edu/user14/csesoft/2018-spring/include
# ---------------------------- END DO NOT EDIT


CPPFLAGS += -std=c++11 -g   # Add your own flags here, or leave blank
LDFLAGS +=                  # Add your own flags here, or leave blank

OBJECTS=irish.o             # Add your own objects here

irish: $(OBJECTS)
	$(LD) $^ $(LDFLAGS) -o $@

%.o: %.cpp
	$(CPP) -I$(INCLUDE) $(CPPFLAGS) -c $<

# Uncomment to use the C compiler
# %.o: %.c
#	$(C) -I$(INCLUDE) $(CFLAGS) -c $<

.PHONY: clean
clean:
	rm -f *.o irish


