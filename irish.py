#!/afs/nd.edu/user14/csesoft/2017-fall/anaconda3/bin/python3.6
# -*- coding: UTF-8 -*-
#nrosa and hgebreme

import sys
import zmq
import time
import datetime

#Create a ZMQ socket to talk to the Server

port = "62400"										#default port which is the lowest port in our range	
context = zmq.Context()								# Contexts help manage any sockets that are created 
socket = context.socket(zmq.PUB)					# The Context (named context here) creates a socket

if len(sys.argv) > 1:
	defaultserver = sys.argv[1]  # takes in the server as input or sets default
else:
	defaultserver = "student00.cse.nd.edu"



print("Writing to port ", port," ")
print("Attempting to connect to "+ defaultserver+ " on port: "+ port)
socket.bind("tcp://*:" +port) 

print ("Connected.")  #socket.setsockopt_string(zmq.SUBSCRIBE, '')		
print ("Welcome to irish")					

while True:																# infinite loop
	try:
		command = input() # takes in input from command line
		socket.send_string(str(command), encoding='utf-8') # sends string with utf-8 encoding 

	except KeyboardInterrupt:
		print(" Caught control-C\n")						#handle the interrupt and exit	
		sys.exit()


if __name__ == "__main__":
	print("Not implemented.")	

